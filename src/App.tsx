import * as React from "react";
import { connect } from "react-redux";
import { FirstArgType, Thunk } from "./types";
import {
    Birthday,
    Birthdays,
    BirthdaysLoadingState,
    selectError,
    selectLoadingState,
    setBirthdays,
    setError,
    setLoadingState,
    setAppState,
    AppState
} from "./redux";

export interface AppStateProps {
    birthdays: AppState["birthdays"];
    error: AppState["error"];
    loadingState: AppState["loadingState"];
}

export interface AppDispatchProps {
    onButtonClick: any;
}

export type AppProps = AppStateProps & AppDispatchProps;

export const App = React.memo((props: AppProps) => {
  return (
    <div className="app">
        <header className="header">
            <h1>Birthday info</h1>
        </header>
        {props.error && <Modal text={props.error} type={"error"} />}
        {props.loadingState === BirthdaysLoadingState.Fetching && <Modal text={"Loading..."} type={"message"} />}
        {props.loadingState !== BirthdaysLoadingState.Fetched && (
            <button
                className="button"
                onClick={props.onButtonClick}
                disabled={props.loadingState !== BirthdaysLoadingState.Unfetched}
            >
                {props.loadingState === BirthdaysLoadingState.Unfetched
                    ? "Fetch birthday data"
                    : "Fetching data..."
                }
            </button>
        )}
        {props.loadingState === BirthdaysLoadingState.Fetched && props.error === undefined && (
            <table className="birthdays">
                <thead>
                    <tr>
                        <td>Year</td>
                        <td>Text</td>
                    </tr>
                </thead>
                <tbody>
                    {props.birthdays?.map((birthday, index) => {
                        return (
                            <tr key={index}>
                                <td>{birthday.year}</td>
                                <td>{birthday.text}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        )}
    </div>
  );
});

export const Modal = React.memo<{ text: string; type: "message" | "error"}>((props) => {
    return (
        <div className={`modal ${props.type}`}>
            <span className="text">
                {props.text}
            </span>
        </div>
    );
});

function mapStateToProps(state: AppState): AppStateProps {
    return {
        error: state.error,
        birthdays: state.birthdays,
        loadingState: selectLoadingState(state),
    };
}

const dispatchProps: AppDispatchProps = { onButtonClick: onButtonClick };

export const AppConnected = connect<AppStateProps, AppDispatchProps, {}, AppState>(mapStateToProps, dispatchProps)(App);

export type OnButtonClickState = FirstArgType<typeof selectError> & FirstArgType<typeof selectLoadingState>;

export function createUrlForCurrentDay(): string {
    const today = new Date();
    const month = today.getMonth() + 1;
    const day = today.getDate();
    const url = `https://api.wikimedia.org/feed/v1/wikipedia/en/onthisday/all/${month}/${day}`;

    return url;
}

export function onButtonClick(): Thunk<OnButtonClickState> {
    return async (dispatch, getState, extraArguments) => {
        let state = getState();

        if (selectLoadingState(state) !== BirthdaysLoadingState.Unfetched) {
            return;
        }

        dispatch(extraArguments.setAppState(setLoadingState(state, BirthdaysLoadingState.Fetching)));

        const birthdaysResult = await extraArguments.fetchJson<{ births: Birthdays; }>(createUrlForCurrentDay());

        state = getState();

        let newState = setLoadingState(state, BirthdaysLoadingState.Fetched);

        if (birthdaysResult.ok) {
            const birthdays = birthdaysResult.value.births;
            birthdays.sort((a: Birthday, b: Birthday) => a.year - b.year);

            newState = setBirthdays(newState, birthdays);

            dispatch(extraArguments.setAppState(newState));
        } else {
            newState = setLoadingState(newState, BirthdaysLoadingState.Fetched);
            newState = setError(newState, birthdaysResult.error);

            dispatch(extraArguments.setAppState(newState));
        }
    };
}
