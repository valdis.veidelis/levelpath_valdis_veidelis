import * as React from "react";
import thunk from "redux-thunk"
import * as ReactDOM from "react-dom/client";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";

import { AppConnected } from "./App";
import { setAppState, appReducer } from "./redux";
import { fetchJson } from "./utils";

import "./index.css";

export function initialize() {
    const root = ReactDOM.createRoot(
        document.getElementById("root") as HTMLElement
    );

    const store = createStore(appReducer, applyMiddleware(thunk.withExtraArgument({
        fetchJson: fetchJson,
        setAppState: setAppState,
    })));


    (window as any).store = store;

    root.render(
        <React.StrictMode>
            <Provider store={store}>
                <AppConnected />
            </Provider>
        </React.StrictMode>
    );
}

document.addEventListener("DOMContentLoaded", initialize);
