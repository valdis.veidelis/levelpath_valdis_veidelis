import merge from "lodash/merge";
import { DeepPartial } from "redux";

import { Thunk } from "./types";

export enum BirthdaysLoadingState {
    Unfetched = "unfetched",
    Fetched = "fetched",
    Fetching = "fetching",
}

export type Year = number;

export interface Birthday {
    pages: Object[];
    text: string;
    year: Year;
}

export type Birthdays = Birthday[];

// export type AppState = {
//     error: undefined;
//     birthdays: undefined;
//     loadingState: BirthdaysLoadingState.Unfetched | BirthdaysLoadingState.Fetching;
// } | {
//     error: undefined;
//     birthdays: Birthday[];
//     loadingState: BirthdaysLoadingState.Fetched;
// } | {
//     error: string;
//     birthdays: undefined;
//     loadingState: BirthdaysLoadingState.Fetched;
// }

export type AppState = {
    error: string | undefined;
    birthdays: Birthday[] | undefined;
    loadingState: BirthdaysLoadingState;
}

export const SET_APP_STATE = "app/SET_APP_STATE";

export function setAppState(state: DeepPartial<AppState>): Thunk<AppState> {
    return (dispatch, getState) => {
        dispatch(setAppStatePlain(merge({}, getState(), state)));
    }
}

export function setAppStatePlain(state: AppState) {
    return {
        type: SET_APP_STATE,
        payload: state,
    } as const;
}

export const initialAppState: AppState = {
    error: undefined,
    birthdays: undefined,
    loadingState: BirthdaysLoadingState.Unfetched,
};

/**
 * Here I demonstrate non-conventional usage of redux which has the following characteristics:
 * - only one action creator for setting redux state
 * - no need to use combineReducers which separate app state in "slices"
 * - multiple dispatches in thunks potentially can be combined in one to improve performance
 * - less boilerplate because instead of action creators and respective reducer, state either can be set directly or
 *      setters can be used to keep traceability of state changes as provided by conventional action creators
 * - if necessary, in redux middlewares one would track state changes not by inspecting action types but with selectors
 *      of prevState/currState instead
 */
export function appReducer(state: AppState = initialAppState, action: SetAppStateAction): AppState {
    if (action.type === SET_APP_STATE) {
        return action.payload;
    }

    return state;
}

export function setError(state: DeepPartial<AppState>, error: AppState["error"]): DeepPartial<AppState> {
    return { ...state, error: error };
}

export function setBirthdays(state: DeepPartial<AppState>, birthdays: AppState["birthdays"]): DeepPartial<AppState> {
    return { ...state, birthdays: birthdays };
}

export function setLoadingState(state: DeepPartial<AppState>, loadingState: AppState["loadingState"]): DeepPartial<AppState> {
    return { ...state, loadingState: loadingState };
}

export type SetAppStateAction = { type: typeof SET_APP_STATE, payload: AppState };

export function selectError(state: Pick<AppState, "error">): AppState["error"] {
    return state.error;
}

export function selectLoadingState(state: Pick<AppState, "loadingState">): AppState["loadingState"] {
    return state.loadingState;
}
