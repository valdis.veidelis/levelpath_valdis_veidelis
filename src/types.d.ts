import { SetAppStateAction } from "./App";
import { fetchJson } from "./utils";
import { setAppState } from "./redux";

export interface ThunkExtraArguments {
    fetchJson: typeof fetchJson;
    setAppState: typeof setAppState;
}

export type Thunk<S, R = void> = ThunkAction<R, S>;

export type ThunkAction<R, S> = (
    dispatch: ThunkDispatch<S>,
    getState: () => S,
    extraArgument: ThunkExtraArguments,
) => R;

export interface ThunkDispatch<S> {
    <R>(asyncAction: ThunkAction<R, S>): R;
    <A extends SetAppStateAction>(action: A): A;
}

export type FirstArgType<T> = T extends (...args: infer A) => any ? A[0] : never;

export type ResultType<T, E = undefined> = { ok: true; value: T } | { ok: false; error: E | undefined };