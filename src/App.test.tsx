import * as React from "react";
import { render, screen } from "@testing-library/react";

import { App, AppProps, createUrlForCurrentDay, onButtonClick } from "./App";
import { Result } from "./utils";
import { AppState, BirthdaysLoadingState, initialAppState, setError } from "./redux";
import { DeepPartial } from "redux";

describe("App", () => {
    test("renders button with initial state", () => {
        const props: AppProps = {
            ...initialAppState,
            onButtonClick: jest.fn(),
        };
        render(<App {...props} />);
        const linkElement = screen.getByText(/Fetch birthday data/i);
        expect(linkElement).toBeInTheDocument();
    });


    test("renders table with birthday data", () => {
        const props: AppProps = {
            birthdays: [{ text: "Some Dude", year: 1234, pages: [] }],
            error: undefined,
            loadingState: BirthdaysLoadingState.Fetched,
            onButtonClick: jest.fn(),
        };

        const { asFragment } = render(<App {...props} />);
        expect(asFragment()).toMatchSnapshot();
    });
})

describe("onButtonClick", () => {
    const defaultState = {
        error: undefined,
        loadingState: BirthdaysLoadingState.Unfetched,
    };

    test("it fetches birthday data and sets correct loading status", async () => {
        const dispatch = jest.fn();
        const setAppState = jest.fn();
        const getState = () => defaultState;
        const thunkExtraArguments = {
            fetchJson: jest.fn().mockReturnValue(new Promise(resolve => {
                resolve(Result.Ok({
                    births: [
                        { text: "text1", year: 1200, pages: [] },
                    ]
                }));
            })),
            setAppState: setAppState,
        };

        const fetchPromise = onButtonClick()(dispatch, getState, thunkExtraArguments);

        expect(thunkExtraArguments.setAppState).toHaveBeenCalledWith({
            loadingState: BirthdaysLoadingState.Fetching,
        });

        await fetchPromise;

        expect(thunkExtraArguments.fetchJson).toHaveBeenCalledWith(createUrlForCurrentDay());
        expect(dispatch).toHaveBeenCalledTimes(2);
        expect(thunkExtraArguments.setAppState).toHaveBeenCalledWith({
            birthdays: [{ text: "text1", year: 1200, pages: [] }],
            loadingState: BirthdaysLoadingState.Fetched,
        });
    });
});

describe("setError", () => {
    test("sets error attribute in state and returns a new reference", () => {
        const state: DeepPartial<AppState> = { error: undefined };
        const newState = setError(state, "some kind of error");

        expect(state === newState).toBe(false);
        expect(newState.error).toBe("some kind of error");
    });
});
