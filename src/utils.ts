import { ResultType } from "./types";


function Ok<T>(data: T): ResultType<T, never> {
    return { ok: true, value: data };
}

function Err<E>(error?: E): ResultType<never, E> {
    return { ok: false, error };
}

export const Result = { Ok: Ok, Err: Err };

export async function fetchJson<T>(url: string): Promise<ResultType<T, string>> {
    let response;

    try {
        response = await fetch(url);
    } catch (e) {
        return new Promise((_, reject) => {
            reject(Result.Err(e))
        });
    }

    if (!response.ok) {
        return Result.Err(response.statusText || `${response.status}`);
    }

    return response.json().then(data => {
        return Result.Ok(data);
    }).catch(error => {
        return Result.Err(error);
    });
}